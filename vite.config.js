export default {
	root: './src/',
	base: './',
	build: {
		target: 'es2017',
		outDir: '../dist/',
		sourcemap: true,
		manifast: true,
		minify: true,
		emptyOutDir: true,
		reportCompressedSize: true,
	},
};
